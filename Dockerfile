FROM python:3
ENV PYTHONBUFFERED=1
WORKDIR /app

RUN pip install --upgrade pip 
COPY ./requirements.txt /app/
RUN pip install -r requirements.txt

COPY ./app/ /app/app/
COPY ./tests/ /app/tests/
COPY setup.py /app/
RUN ls -al .
RUN pip install -e /app/

EXPOSE 80
CMD ["flask", "run", "--host=0.0.0.0", "--port=80"]

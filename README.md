# Appointment Manager

A minimal appointment manager built with Python and Flask.

## Install and run 

###  Run in a docker container

On any system running Docker, this app can be run with:

```
docker run -d -p 8000:80 ariromerog/appointments:1.0
```

the app will be available on http://localhost:8000

###  Run on virtualenv

Alternatively, on Linux/Unix systems, with python 3, this app can be run in a Python virtualenv, by default the app will be available on http://localhost:5000

```
virtualenv venv
pip install -r requirements.txt
source venv/bin/activate
cd app
flask run 
```

## API Usage

### List user's appointments

To list all user's appointments, use `GET /appointments?user=<user id>`. Expected response is:

```
{
	"appointments":[
		"2021-10-05T00:30"
	]
}
```


### Create an appointment 

To create an appointment, use `POST /appointments` in JSON format, `user` is an integer number and `datetime` is date/time in ISO format e.g. `YYYY-mm-ddThh:mi`, minutes can be either `00` or `30`. If the request doesn't follow this standard, the server will return an http `400` status. If the user has already made an appointment for that date, the server will return an http `409` status.

```
POST /appointments
{
	"user": 1,
	"datetime": "2022-01-01T08:00"
}
```

Expected response is:
```
{"msg": "OK"}
```
## TODO
- Include unit tests
- Include functionality tests








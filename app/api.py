from flask import ( 
        Blueprint, request, render_template, jsonify
        )
from re import match, split
from . import appointments

bp = Blueprint('api', __name__, url_prefix='/')
apmanager = appointments.AppointmentManager()

@bp.route('/')
def index():
    return render_template('index.html')


@bp.route('/appointments', methods=['GET'])
def get_appointemnts():
    user = request.args.get('user')
    appointments = []

    if user is not None:
        appointments = apmanager.getByUser(user)

    return jsonify({'appointments': appointments})


@bp.route('/appointments', methods=['POST'])
def create_appointment():
    appointment = request.get_json(force=True)
    dt_pattern = '\d\d\d\d-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])T([0-1][0-9]|2[0-3]):(00|30)'

    if 'user' not in appointment or 'datetime' not in appointment:
        return jsonify({'msg': 'user and datetime  are required' }), 400

    if not bool(match(dt_pattern, appointment['datetime'])):
        return jsonify({'msg': 'Bad date format' }), 400

    user = str( appointment['user'] )
    date, time = split('T', appointment['datetime']) 

    if not apmanager.isAvailable(user, date):
        return jsonify({'msg': 'Date not available' }), 409

    if apmanager.add(user, date, time):
        return jsonify({'msg': 'OK' })
    
    return jsonify({'msg': 'error' }), 500



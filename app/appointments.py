class AppointmentManager(object):
    """
    Appointments
    ------------
    manages medical apdb
    - apdb have user and date, and time
    - users are 1 to N
    - dates are yyyy-mm-dd
    - times are hh:00 or hh:30
    - for now, everything is used as strings 
    - user can not have more than one appointment for a date

    Appointments wil be saved in a dict, in the following fashion:
    for faster access.

    {
        'user_id' : {
                'date': 'time'
            }
        } 
    }

    """
    def __init__(self):
        self.apdb = {} 
        
    def add(self, user, date, time):
        
        if not self.isAvailable(user, date):
            return False 

        if user not in self.apdb:
            self.apdb[user] = {}

        self.apdb[user][date] = time

        print(self.apdb)

        return True


    def isAvailable(self, user, date):
        if user in self.apdb:
            if date in self.apdb[user]:
                return False
        return True 

    def getByUser(self, user):
        if user in self.apdb:
            ret = []
            for d,t in self.apdb[user].items():
                ret.append('{}T{}'.format(d,t))
            return ret
        return [] 


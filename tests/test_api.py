from app import create_app
from flask import json

def test_index(client):
    # check that there is an index page
    response = client.get('/')
    assert response.status_code == 200

def test_no_appointments(client):
    # an empty list should be shown on the first invocation
    response = client.get('/appointments')
    data = json.loads(response.data)
    assert response.status_code == 200
    assert 'appointments' in data
    assert data['appointments'] == [] 


def test_new_appointment(client):
    # test creation of one appointment
    data = {
            "user": 1,
            "datetime": "2020-10-01T00:30"
    }
    
    response = client.post('/appointments',
        data = json.dumps(data)
    )
    
    assert response.status_code == 200

def test_new_appointment_bad_date_format(client):
    # test creation of one appointment with bad date format (month 14)
    data = {
            "user": 1,
            "datetime": "2020-14-01T00:30"
    }
    
    response = client.post('/appointments',
        data = json.dumps(data)
    )
    
    assert response.status_code == 400
    
def test_new_appointment_same_day(client):
    # test creation of two appointments on the same day
    data = {
            "user": 2,
            "datetime": "2020-10-01T00:30"
    }
    
    response = client.post('/appointments',
        data = json.dumps(data)
    )
    
    assert response.status_code == 200

    data = {
            "user": 2,
            "datetime": "2020-10-01T10:30"
    }
    
    response = client.post('/appointments',
        data = json.dumps(data)
    )

    assert response.status_code == 409
    
    data = {
            "user": 2,
            "datetime": "2020-10-02T10:30"
    }
    
    response = client.post('/appointments',
        data = json.dumps(data)
    )

    assert response.status_code == 200


def test_list_appointments(client):
    # two appointments should have been created 
    response = client.get('/appointments?user=2')
    data = json.loads(response.data)
    assert response.status_code == 200
    assert 'appointments' in data
    assert data['appointments'] != []
    assert len(data['appointments']) == 2

